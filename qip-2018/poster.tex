\documentclass[25pt, a0paper, portrait, margin=0mm, innermargin=10mm, blockverticalspace=5mm, colspace=10mm, subcolspace=8mm]{tikzposter}

\usepackage{authblk}
\makeatletter
\renewcommand\maketitle{\AB@maketitle} % revert \maketitle to its old definition
\renewcommand\AB@affilsepx{\quad\protect\Affilfont} % put affiliations into one line
\makeatother
\renewcommand\Affilfont{\Large} % set font for affiliations

\title{A limitation on the asymptotic decay of vanishing spectral gaps}
\author[1]{Michael J. Kastoryano}
\author[2,1]{Angelo Lucia}
\affil[1]{Niels Bohr International Academy, University of Copenhagen}
\affil[2]{QMATH, Department of Mathematical Sciences, University of Copenhagen}
\institute{\texttt{arXiv:1705.09491}}

\usetheme{Simple}
\usecolorstyle{Denmark}

\usepackage{physics}
\usepackage{dsfont}
\usepackage{graphicx}
\graphicspath{{./../../}}

\newcommand{\R}{\mathds{R}}
\DeclareMathOperator{\diam}{diam}

\usepackage[doi=false]{biblatex}
\addbibresource{./../../biblio.bib}

\begin{document}
\maketitle[width=80cm]

\begin{columns}
  \column{0.5}
\block{Introduction}
{

    We consider many-body quantum systems of qudits located on the vertices of a ``flat'' lattice
    $\Lambda \subset \R^D$, and a local finite-range \emph{frustration-free} Hamiltonian
    \[ H_\Lambda = \sum_{Z \subset \Lambda} h_Z \] We will assume that $h_Z$ are \emph{orthogonal
      projections} (it is not a restrictive assumption if $e\le h_Z \le E$ for some system-size
    independent $e,E \in \R$).



    \innerblock{Spectral gap}{
      The \emph{spectral gap} $\lambda_\Lambda$
      of $H_\Lambda$ is the difference between the two lowest distinct eigenvalues of $H_\Lambda$.
      }

}
\column{0.5}
\block{Main result}
{
  The Hamiltonian is said to be \emph{gapped} it $\lambda_\Lambda$ is lower bounded by a constant
    independent of $\Lambda$, and is \emph{gapless} if $\lambda_\Lambda \to 0$ as $\abs{\Lambda}$ goes to
    infinity.
  Let $n=\diam{\Lambda}$.
  There is a dichotomy: one of the following is true
  \innerblock{}{
  \begin{enumerate}
  \item The Hamiltonian is gapped: $\lambda_\Lambda = \order{1}$;
  \item The Hamiltonian is gapless and $\lambda_\Lambda = o\qty(\frac{log(n)^{2+\epsilon}}{n})$ for
    every $\epsilon>0$.
  \end{enumerate}
  }
  In particular, if we can prove a lower bound to the gap which is vanishing
  \emph{slower} than the one given above, then the Hamiltonian is actually gapped!
  \vspace{0.5em}
  \coloredbox{The result is independent of the dimension of the lattice $D$!}  }

\end{columns}

\begin{columns}
  \column{0.5}

    \block{The martingale condition \dots}{

    We consider two overlapping sets $A$ and $B$, and $P_A$ and $P_B$ the respective groundstate projectors.
    \begin{align*}
      \delta(A,B) &= \norm{(P_A-P_{A\cup B})(P_B - P_{A\cup B})} \\
      &= \norm{P_A P_B - P_{A\cup B}}
    \end{align*}
    Let $d$ is the size of the overlap $A\cup B$. We want the following equation to be satisfied:
    \begin{equation}\label{eq:martingale}
      \delta(A,B) \le \delta(d)
    \end{equation}

    \vspace{1em}
    \innerblock{Strong version}{
     Eq.~\eqref{eq:martingale} is satisfied uniformly for all $A$ and $B$ with $\delta(d)$ decaying
     exponentially in $d$.
   }
   \vspace{1em}
   \innerblock{Weak version}{
     Eq.~\eqref{eq:martingale} is satisfied for all $A$ and $B$ of size $\order{d^{3/2}}$ with
     $\delta(d)$ decaying polynomially in $d$.}

 }



\block{\dots implies a spectral gap}{
  \innerblock{Theorem}
  {
    If the weak martingale condition is satisfied, then the Hamiltonian is gapped.
  }
  \vspace{1em}
  \innerblock{}{
    {\bf Sketch of the proof}:
    the spectral can be characterized as the optimal constant such that
    $ P_\Lambda^\perp \le \frac{1}{\lambda_{\Lambda}} H_{\Lambda}$
    holds. Then given two overlapping reagions $A$
    and $B$, we can prove that
    \begin{equation}
      \qty(1-2\delta(A,B)) P_{A\cup B}^\perp \le P_A^\perp + P_B^\perp.
    \end{equation}
    We can then relate the spectral gap of $H_A$ and $H_B$ to the spectral gap of $H_{A\cup B}$:
    \[ P_{A\cup B}^\perp \le \frac{1}{1-2\delta(A,B)} \cdot \frac{1}{\min(\lambda_A,\lambda_B)}[ H_{A\cup
        B} + H_{A\cap B}] \]
    How to treat the extra term $H_{A\cap B}$? If we bound it by $H_{A\cup B}$, we obtain a
    recursive relation which gives an exponentially vanishing bound:
    \[ \lambda_{A\cup B} \ge \frac{1-2\delta(A,B)}{2}\min(\lambda_A,\lambda_B) . \]
    \vspace{-1em}
    \begin{tikzfigure}
      \includegraphics[scale=0.75]{Fig2}
    \end{tikzfigure}

    Instead, given $\Lambda$, we will choose $s$ different decompositions $\Lambda=A_i\cup B_i$,
    such that $A_i\cap B_i$ are all disjoint, and
    average over all of them, obtaining:
    \begin{equation}
      \lambda_{\Lambda} \ge \frac{1-2\min_i \delta(A_i,B_i)}{1+1/s} \min_i
      \min(\lambda_{A_i},\lambda_{B_i}).
    \end{equation}
    Now we just need to find the right trade-off between
    \begin{enumerate}
    \item the number of partitions $s$ $\Rightarrow$ $\prod (1+1/s) <\infty$;
    \item the size of the overlap $A_i \cap B_i$ $\Rightarrow$ $\prod (1-2\min_i\delta(A_i,B_i)) > 0$;
    \item the size of the regions $A_i$, $B_i$ $\Rightarrow$ the number of steps in the recursion is small.
    \end{enumerate}
    We can do this by choosing exponentially growing regions, and the weak martingale condition is
    sufficient to prove convergence to a positive bound.

}
}

% \block{A geometrical construction}
% {

%     \begin{tikzfigure}
%       \includegraphics[scale=0.75]{Fig2}
%     \end{tikzfigure}

%    This is a generalization of a construction from \cite{Cesi2001}.

%     We define $l_k = (3/2)^k$ and we define $\mathcal F_k$ the set of $\Lambda$ contained in
%     \[ R(k) = [0,l_{k+1}]\times \cdots \times [0,l_{k+D}] \]
%     up to translations and reordering of the axes.

%     Each set $\Lambda \in \mathcal F_k$ can be decomposed in $s_k \le \frac{1}{8}l_k$ different ways as a
%     union of $A_i$, $B_i$ such that:

%     \begin{enumerate}
%     \item $A_i$, $B_i$ are not empty and belong to $\mathcal F_{k-1}$;
%     \item $A_i \cap B_i$ have size $d_k = \frac{l_k}{8s_k}$;
%     \item $A_i\cap B_i$ are disjoint for different $i$.
%     \end{enumerate}

%     Let us denote $\lambda_{l_k} = \inf_{\Lambda \in \mathcal F_k} \lambda_\Lambda$ and
%     $\delta_{d_k} = \sup_{\Lambda \in \mathcal F_k} \sup_{A_i, B_i} \delta(A_i,B_i)$.
%     Then
%     \begin{equation}\label{eq:recursion}
%       \lambda_{l_k} \ge \frac{1-2\delta_{d_k}}{1+1/s_k} \lambda_{l_k-1}.
%     \end{equation}

%     By choosing $s_k = l_k^{1/3}$ we get $d_k = \order{l_k^{2/3}}$ and the weak martingale condition
%     is sufficient to show that $\sum_k \delta_{d_k} < \infty$.
%   }


   \column{0.5}

  \block{Detectability lemma}{
  As a consequence of the Detectability Lemma, we have that
  \vspace{0.5em}
  \innerblock{Lemma}{
    If $\lambda_\Lambda$ is the spectral gap of $H_\Lambda$, then
    \begin{equation}\label{eq:detectability}
     \delta(A,B) \le \frac{1}{\qty(1+\lambda_\Lambda/g^2)^{d/2}}
    \end{equation}
    for all $A$ and $B$ with overlap $d$, for some positive integer $g$.
  }
  % \innerblock{}{
  %   {\bf Sketch of the proof:} We consider the approximate ground state projector $L$, given by the
  %   Detectability lemma, and then split $L^{d} = M_A M_B$ as follows.
  %   \begin{tikzfigure}
  %     \includegraphics[scale=0.75]{Fig3}
  %   \end{tikzfigure}
  %   Then $M_A$ and $M_B$ are good approximations of $P_A$ and $P_B$, and we can prove that
  %   \[
  %     \norm{L^{d} - P_{A\cup B}} \le \epsilon^{d} \quad
  %     \norm{(P_A-M_A)M_B} \le \epsilon^{d} \quad
  %   \]

  % }
  \vspace{1em}
  \innerblock{Theorem}
  {
    If the Hamiltonian is gapped, then it satisfies the strong martingale condition.
  }
}


\block{An equivalence relationship}{
The two theorems show that the following are equivalent
\begin{enumerate}
\item The Hamiltonian is gapped;
\item The strong martingale condition;
\item The weak martingale conditon.
\end{enumerate}

\innerblock{}{The martingale condition is {\bf self-improving}: but because of this equivalence,
  bounds on the spectral gap are self-improving too!}

  % We can plug Eq.~\eqref{eq:detectability} into Eq.~\eqref{eq:recursion}, and obtain:
  % \[ \lambda_{l_k} \ge \frac{1-2\mu^{\frac{l_k}{s_k} \lambda_{l_k}}}{1+1/s_k} \lambda_{l_k-1} \]
  % for some $\mu\in(0,1)$.
  % This gives a converging bound if
  % \[ \sum_k \frac{1}{s_k} < \infty, \quad \liminf_k \frac{l_k}{k s_k}\lambda_{l_k}  > 0. \]
  % By choosing $s_k = k^{1+\epsilon}$ for some $\epsilon >0$, we obtain that if
  % \[ \lambda_{l_k} >  C \frac{\log(l_k)^{2+\epsilon}}{l_k}\quad \forall k\ge k_0, \]
  % then we can prove a system size independent lower bound $\lambda_{l_k} = \order{1}$.
\vspace{1em}
\innerblock{Theorem}{
  If there exists positive constants $C$, $\epsilon$, and $n_0$ such that $ \lambda_\Lambda \ge C
  \frac{\log^{2+\epsilon}(n)}{n}$ for all $n\ge n_0$, then $\lambda_\Lambda = \order{1}$.
}
}

\block{Comparing with previous results}
{
  \innerblock{Local gap threshold}
  {
    For 1D and some 2D lattices with nearest-neighbor interactions, there exist a threshold
    $\gamma_n$ such that, if $\lambda_{n_0} \ge \gamma_{n_0}$ for \emph{some} $n_0$, then $\lambda_n
    = \order{1}$ for all $n$.
  }
  \vspace{1em}
  \begin{center}
  \begin{tabular}{*4c}
    \hline\hline
     1D \cite{Knabe1988} & 1D \cite{Gosset2016} & 2D hexagonal \cite{Knabe1988} & 2D square
                                                                                  \cite{Gosset2016}
    \\ \hline
     $\frac{1}{n-1}$ & $\frac{6}{n(n+1)}$ & $\frac{1}{3n-1}$ & $\frac{8}{n^2}$
  \end{tabular}
\end{center}
\vspace{1em}
{\bf Open problems:}
\begin{enumerate}
\item Is the optimal bound $\order{1/n^2}$ in general? Or does it only holds for 2-body
  interactions?
\item Can we get a ``single point'' threshold instead of an asymptotic bound in any dimension?
\end{enumerate}
\vspace{1em}
\innerblock{References}{
  \coloredbox{
    \printbibliography[heading=none]
    }
  }
  }
\end{columns}


\block{}{
 % \vspace{-2em}
  \begin{tikzfigure}
    \includegraphics[height=3em]{qmath}
    \hspace{2em}
    \includegraphics[height=5em]{erc}
%  \end{tikzfigure}
%  \begin{tikzfigure}
    \hspace{3em}
    \includegraphics[height=5em]{dff}
    \hspace{3.5em}
    \includegraphics[height=3em]{villum}
  \end{tikzfigure}
}


\end{document}
