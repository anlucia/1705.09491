\documentclass{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usetheme{material}
\usePrimaryTeal

\usepackage{physics}
\newcommand{\gap}[1][\Lambda]{\mu_{#1}}
\DeclareMathOperator{\diam}{diam}

\usetikzlibrary{calc,decorations.pathmorphing}
\tikzset{
  onslide/.code args={<#1>#2}{ \only<#1>{\pgfkeysalso{#2}}},
  invisible/.style={opacity=0,text opacity=0, fill opacity=0},
  visible on/.style={alt=#1{}{invisible}},
  alt/.code args={<#1>#2#3}{%
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}}
  }
}
\tikzset{
  spin/.style={shape=circle,draw=blue!70,fill=blue!50},
  entanglement/.style={thick, decorate, decoration=snake},
}

\graphicspath{{./figures/}}

\title{It can't get any slower}
\author[A. Lucia]{Angelo Lucia}
\date{N-talk March 9th 2018}

\begin{document}
\maketitle

\section{The problem}
\begin{frame}{Quantum spin system}
  \begin{card}
    \centering
    \begin{tikzpicture}
      \foreach \x in {0,...,4}
      \foreach \y in {0,...,4}
      {\node [spin] (\x\y) at (\x,\y) {};}

      \foreach \x in {0,...,4}
      \foreach \y [count=\yi] in {0,...,3}
      \draw (\x\y)--(\x\yi) (\y\x)--(\yi\x) ;

      \node[right] at (43.east) {$\Lambda$};
    \end{tikzpicture}
  \end{card}
  \begin{cardTiny}
    $H_\Lambda$ local (finite range) Hamiltonian on $\Lambda$.

    Energy gap:
    $\gap = E_1(\Lambda) - E_0(\Lambda)$.
  \end{cardTiny}
\end{frame}

\begin{frame}{Gapped vs. gapless}
  \begin{card}
    We distinguish two cases:
    \begin{enumerate}
    \item $\gap\ge \gamma > 0$ for all $\Lambda$ (\alert{gapped});
    \item $\gap \to 0$ as $\abs{\Lambda}\to \infty$ (\alert{gapless}).
    \end{enumerate}
  \end{card}
  \begin{card}[Question:]
    If the system is \emph{gapless}, what asymptotic behaviour can $\gap$ exhibit (as a function of
    $\diam \Lambda$)?
  \end{card}
\end{frame}

\section{The result}
\begin{frame}
  \begin{card}[Frustration free]
    The system is \alert{frustration free} if $E_0(\Lambda) = 0$ and
    $H_\Lambda$ is a sum of positive terms with a common groundstate.  In this case
    \[0\cdot P_\Lambda + \gap P_\Lambda^\perp = \gap P_\Lambda^\perp \le H_\Lambda \]\vspace{-2em}
  \end{card}

  \begin{cardTiny}
    Classical anti-ferromagnetic Ising interaction on a triangular lattice is \alert{frustrated}:
    \begin{figure}
      \begin{tikzpicture}
        \node[spin,inner sep =1pt,fill=green] (A) at (0,0) {$\uparrow$};
        \node[spin,inner sep =1pt,fill=yellow] (B) at (2,0) {$\downarrow$};
        \node[spin,inner sep =1pt,fill=green] (C) at (1,1.7) {$\uparrow$};
        \draw[thick] (A) -- (B) -- (C) -- (A);
        \draw[red,thick] (A) -- (C);
      \end{tikzpicture}
    \end{figure}
\end{cardTiny}
\end{frame}

\begin{frame}
  \begin{card}[Dichotomy]
    If the system is frustration free, then either
    \begin{enumerate}
      \item (\alert{gapped}) $\gap\ge \gamma > 0$ for all $\Lambda$;
      \item (\alert{gapless}) $\gap =o\qty(\frac{\log(n)^{\frac{5}{2}}}{n})$, where $n = \diam \Lambda$.
      \end{enumerate}
      This is independent on the interaction, of the geometric dimension of $\Lambda$, of the local
      spin dimension.
    \end{card}

    \begin{card}
      If we can lower bound $\gap$ (asymptotically) by a function slower than that, then the system
      is gapped!
    \end{card}

\end{frame}

\section{Sketch of the proof}

\begin{frame}{Recursive strategy}
  \begin{cardTiny}
    \centering
    \begin{tikzpicture}[scale=0.5]
      \draw[fill=red, fill opacity=0.2] (-0.5,-0.5) rectangle (6.5,4.5);
      \draw[fill=blue, fill opacity=0.2] (3.5,-0.5) rectangle (10.5,4.5);
      \draw[<->] (-0.5,5) -- (6.5,5) node[midway, above] {$A$};
      \draw[<->] (3.5,-1) -- (10.5,-1) node[midway, below] {$B$};

      \foreach \x in {0,...,10}
      \foreach \y in {0,...,4}
      {\node [spin, inner sep=2pt] (\x\y) at (\x,\y) {};}

      \foreach \x [count=\xi] in {0,...,9}
      \foreach \y in {0,...,4}
      \draw (\x\y)--(\xi\y);

      \foreach \x in {0,...,10}
      \foreach \y [count=\yi] in {0,...,3}
      \draw (\x\y) -- (\x\yi);

      \node[left] at (00.west) {$\Lambda$};

      \node<2>[spin,fill=red,inner sep=2pt] at (2,2) {};
      \node<2>[spin,fill=red,inner sep=2pt] at (4,3) {};
      \node<2>[spin,fill=red,inner sep=2pt] at (0,4) {};
      \draw<2>[entanglement, red] (2,2) -- (4,3) -- (0,4) -- (2,2);

      \node<3>[spin,fill=green,inner sep=2pt] at (6,2) {};
      \node<3>[spin,fill=green,inner sep=2pt] at (9,0) {};
      \node<3>[spin,fill=green,inner sep=2pt] at (7,4) {};
      \draw<3>[entanglement, green] (6,2) -- (7,4) -- (9,0) -- (6,2);

      \node<4->[spin,fill=red,inner sep=2pt] (a) at (2,2) {};
      \node<4->[spin,fill=green,inner sep=2pt] (b) at (9,0) {};
      \draw<4->[entanglement,red] (a) -- ($(a)!0.5!(b)$);
      \draw<4->[entanglement,green] ($(a)!0.5!(b)$) -- (b);

      %\draw<3->[<->,thick, red] (3.5,2) -- (6.5,2) node[midway, above] {$d$};
    \end{tikzpicture}
  \end{cardTiny}
  \begin{overlayarea}{\textwidth}{0.3\textheight}
  \begin{cardTiny}
    \only<1>{
      We split $\Lambda=A\cup B$.
      We would like to relate the gap $\gap$ with the gaps $\gap[A]$ and $\gap[B]$,
      by comparing $\expval{H_\Lambda}{\psi}$ with $\expval{H_A}{\psi}$ and $\expval{H_B}{\psi}$.
  }
  \only<2>{
    Let us consider an excited state $\ket{\psi}$. If it has excitations
    localized in $A$, then it will have energy at least $\gap[A]$.
    \[  \gap[A] \braket{\psi} \le \expval{H_A}{\psi} \le \expval{H_\Lambda}{\psi} \]\vspace{-1em}
  }
    \only<3>{
      The same argument works for $B$: if excitations are localized in $B$, their energy is at least
      $\gap[B]$.
      \[ \gap[B]\braket{\psi} \le \expval{H_B}{\psi} \le \expval{H_\Lambda}{\psi} \]\vspace{-1em}
    }
    \only<4>{
      What if a state has excitations which are \alert{not localized} (i.e. a superposition between
      excitations in $A\setminus B$ and $B\setminus A$)?

      \[ \expval{H_A + H_B}{\psi} \ge \gap[A]\expval{P_A^\perp}{\psi} + \gap[B] \expval{P_B^\perp}{\psi}  \]\vspace{-1em}

    }
  \end{cardTiny}
\end{overlayarea}
\end{frame}
\begin{frame}{Groundstates overlap}
  \begin{cardTiny}
    \centering
    \begin{tikzpicture}[scale=0.5]
      \draw[fill=red, fill opacity=0.2] (-0.5,-0.5) rectangle (6.5,4.5);
      \draw[fill=blue, fill opacity=0.2] (3.5,-0.5) rectangle (10.5,4.5);
      \draw[<->,visible on=<1>] (-0.5,5) -- (6.5,5) node[midway, above] {$A$};
      \draw[<->,visible on=<1>] (3.5,-1) -- (10.5,-1) node[midway, below] {$B$};

      \foreach \x in {0,...,10}
      \foreach \y in {0,...,4}
      {\node [spin, inner sep=2pt] (\x\y) at (\x,\y) {};}

      \foreach \x [count=\xi] in {0,...,9}
      \foreach \y in {0,...,4}
      \draw (\x\y)--(\xi\y);

      \foreach \x in {0,...,10}
      \foreach \y [count=\yi] in {0,...,3}
      \draw (\x\y) -- (\x\yi);

      \node[left] at (00.west) {$\Lambda$};

      \node[spin,fill=red,inner sep=2pt] (a) at (2,2) {};
      \node[spin,fill=green,inner sep=2pt] (b) at (9,0) {};
      \draw[entanglement,red] (a) -- ($(a)!0.5!(b)$);
      \draw[entanglement,green] ($(a)!0.5!(b)$) -- (b);

      \draw<2->[<->,thick] (3.5,4.75) -- (6.5,4.75) node[midway, above] {$d$};
    \end{tikzpicture}
  \end{cardTiny}
  \begin{overlayarea}{\textwidth}{0.3\textheight}
    \begin{cardTiny}
      \only<1>{
        \[ \expval{P_A^\perp}{\psi} +  \expval{P_B^\perp}{\psi} \ge (1-\delta)\braket{\psi} \]
      where $\delta = 2\norm{(P_A-P_\Lambda)(P_B-P_\Lambda)}$ measures how close to a
      groundstate in $A$ and $B$ can an excited state be.
    }
    \only<2>{
      \alert{Idea:} $\delta$ is exponentially small in $d\gap$.
      \[ \expval{P_A^\perp}{\psi} +  \expval{P_B^\perp}{\psi} \ge(1- 2e^{-c d \gap})\braket{\psi} \]
    }
  \end{cardTiny}
\end{overlayarea}
\end{frame}

\begin{frame}
  \begin{card}
    In conclusion
    \[ (1-2e^{-cd\gap})\min(\gap[A],\gap[B]) \le \expval{H_A+H_B}{\psi} .\]
  \end{card}
  \begin{card}
    Since $H_A+H_B = H_\Lambda + H_{A\cap B} \le 2H_\Lambda$, we get the recursive relationship
    \[
      \frac{1-2e^{-c d \gap}}{\alert<2>{2}} \min(\gap[A],\gap[B]) \le \gap.
    \]
    \uncover<2>{
      Because of the factor of $\frac{1}{2}$, if we do the recursion, the bound will vanish exponentially fast!
    }
  \end{card}
\end{frame}

\begin{frame}{Averaging trick}
    \begin{cardTiny}
    \centering
    \begin{tikzpicture}[scale=0.5]

      \foreach \d [count=\n] in {0,1,2}{
      \draw[fill=red, fill opacity=0.2, visible on=<\n>] (-0.5,-0.5) rectangle ($ (4.5,4.5) + \d*(2,0) $);
      \draw[fill=blue, fill opacity=0.2,  visible on=<\n>]  ($ (2.5,4.5) + \d*(2,0) $)  rectangle
      (10.5,-0.5);
      \draw[fill=gray, fill opacity=0.2, visible on=<4->]  ($ (2.5,-0.5) + \d*(2,0) $) rectangle ($ (4.5,4.5) + \d*(2,0) $);
      \draw[<->, visible on=<\n>] (-0.5,5) --  ($ (4.5,5) + \d*(2,0) $) node[midway, above] {$A_\n$};
      \draw[<->, visible on=<\n>] ($ (2.5,-1) + \d*(2,0) $) -- (10.5,-1) node[midway, below] {$B_\n$};
      \draw[<->, visible on=<4->] ($ (2.5,5) + \d*(2,0) $) -- ($ (4.5,5) + \d*(2,0) $) node[midway, above]
      {\tiny $A_\n\cap B_\n$};
    }

      \foreach \x in {0,...,10}
      \foreach \y in {0,...,4}
      {\node [spin, inner sep=2pt] (\x\y) at (\x,\y) {};}

      \foreach \x [count=\xi] in {0,...,9}
      \foreach \y in {0,...,4}
      \draw (\x\y)--(\xi\y);

      \foreach \x in {0,...,10}
      \foreach \y [count=\yi] in {0,...,3}
      \draw (\x\y) -- (\x\yi);

    \end{tikzpicture}
  \end{cardTiny}
  \begin{overlayarea}{\textwidth}{0.3\textheight}
  \begin{cardTiny}
    \only<-4>{
    We can split $\Lambda$ in $k$ different ways (keeping $d$ constant)
    \[ A_1 \cup B_1 = A_2 \cup B_2 = \dots = A_k \cup B_k \]
    such that $A_i \cap B_i \cap A_j \cap B_j = \emptyset$ whenever $i\neq j$
    }
\only<5>{
    \[ \frac{1}{k}\sum_{i=1}^k \qty(H_{A_i} + H_{B_i}) = H_{\Lambda} + \frac{1}{k}\sum_{i=1}^k H_{A_i \cap
        B_i} \le \qty(1+\frac{1}{k}) H_\Lambda  \]
    }
  \end{cardTiny}
\end{overlayarea}

\end{frame}

\begin{frame}{The recursion}
  \begin{card}
    \[
      \frac{1-2e^{-c d \gap}}{1+1/k} \min_i \min(\gap[{A_i}],\gap[{B_i}]) \le \gap.
    \]
    If $\gap$ is larger than $\frac{\log(n)^{5/2}}{n}$ then this gives a constant lower bound.
  \end{card}

  \begin{card}[Open question: is the threshold tight?]
    For 1D and 2D models, we now the optimal threshold is smaller: $\order{1/n^2}$ for periodic b.c. and
    $\order{1/n^{3/2}}$ for open b.c.
    \end{card}
\end{frame}


\begin{frame}

  \begin{card}
    \centering
    \textbf{Thank you for your attention! (Questions?)}
  \end{card}

  \begin{card}[Reference]
    M.\ J.\ Kastoryano and A.\ L.

    \emph{Divide and conquer method for proving gaps of frustration free Hamiltonians}

    JSTAT (to appear).
  \texttt{arXiv:1705.09491 [math-ph]}
\end{card}

\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
