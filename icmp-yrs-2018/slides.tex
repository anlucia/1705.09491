\documentclass[compress]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usetheme{metropolis}

\usepackage{physics}
\newcommand{\gap}[1][\Lambda]{\mu_{#1}}
\DeclareMathOperator{\diam}{diam}

\newcommand{\ie}{i.\,e.\ }
\newcommand{\eg}{e.\,g.\ }

% math fonts
\usepackage{dsfont}
\newcommand{\mcl}{\mathcal}

% fields and rings
\newcommand{\field}[1]{\ensuremath{\mathds{#1}}}
\newcommand{\hs}{\mcl H}
\newcommand{\id}{\mathds{1}}

\usetikzlibrary{calc,decorations.pathmorphing}
\tikzset{
  onslide/.code args={<#1>#2}{ \only<#1>{\pgfkeysalso{#2}}},
  invisible/.style={opacity=0,text opacity=0, fill opacity=0},
  visible on/.style={alt=#1{}{invisible}},
  alt/.code args={<#1>#2#3}{%
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}}
  },
}
\tikzset{
  spin/.style={shape=circle,draw=blue!60, fill=blue!50, scale=0.4},
  entanglement/.style={thick, decorate, decoration=snake},
}

\graphicspath{{./figures/}}

\titlegraphic{\hfill \includegraphics[height=30pt]{qmath-center.pdf}}
\title{A limitation\\on the asymptotic decay\\of vanishing spectral gaps}
\subtitle{\texttt{1705.09491}}
\author[A. Lucia]{Angelo Lucia (QMATH, University of Copenhagen) \\
  joint work with M.~J.~Kastoryano (University of Cologne)
}
\date{\vspace{3em} \hfill YRS 2018, July 21st}

\begin{document}
\maketitle

\section{Gapped and gapless Hamiltonians}

\begin{frame}{Quantum spin systems}

  \begin{columns}[c]

    \column{0.25\textwidth}

    \begin{tikzpicture}
      \node[circle, minimum size=1cm, draw=blue!60, fill=blue!20 ] at (0.75,0.75) {$h$};
      \draw[step=.5cm,densely dotted] (-1.4,-1.4) grid (1.4,1.4);
      \foreach \x in {-1,-0.5,...,1}
      \foreach \y in {-1,-0.5,...,1} {
        \draw (\x, \y) node[spin] {};
        \draw (\y, \x) node[spin] {};
      }
    \end{tikzpicture}

    \column{0.75\textwidth}

    \begin{itemize}
      \item Ingredients:
        \begin{enumerate}
        \item $\Gamma = \field{Z}^D$
        \item spin $s$ particles with Hilbert space $\hs_d = \field C^{2s+1}$
        \item a finite-range interaction $Z \subset \subset \Gamma \mapsto h(Z)$
          ($h(Z) =0$ if $\diam Z > r^*$)
        \end{enumerate}
      \item Recipe:
        \begin{enumerate}
        \item $\Lambda$ a finite subset of $\Gamma$
        \item Hamiltonian (o.b.c.) on $\Lambda$ is given by \[ H_\Lambda = \sum_{Z\subset \Lambda} h(Z)\]
        \end{enumerate}
      \end{itemize}
  \end{columns}

  \pause
      \begin{definition}[spectral gap]
    The \alert{spectral gap} $\gap = E_1(\Lambda) - E_0(\Lambda)$ of $H_\Lambda$ is the difference between the two smallest eigenvalues
    (without multiplicities).
  \end{definition}

\end{frame}

\begin{frame}{Gapped vs. gapless}
  \begin{columns}[c]

    \column{0.5\textwidth}

    When $\Lambda \nearrow \Gamma$, we distinguish two cases:
    \begin{description}
    \item[\alert<1>{Gapped:}] $\liminf_{\Lambda} \gap > 0$
    \item[\alert<1>{Gapless:}] $ \liminf_{\Lambda} \gap = 0$
    \end{description}


    \column{0.5\textwidth}
    \pause
    \begin{alertblock}{Warning}
      Spectral gap of the GNS Hamiltonian is lower bounded (but not necessarily
      equal) to $\liminf_{\Lambda} \gap$!
    \end{alertblock}

  \end{columns}
  \vspace{3em}
  \begin{exampleblock}{Question}
    If the system is \emph{gapless}, what asymptotic behaviour can $\gap$ exhibit (as a function of
    $\diam \Lambda$)?
  \end{exampleblock}
\end{frame}

\section{The result}
\begin{frame}{Frustration free}
  \begin{definition}
    The system is \alert{frustration free} if a groundstate of $H_\Lambda$ is also a groundstate of
    $h(Z)$ for every $Z\subset \Lambda$.
  \end{definition}

    For frustration free interactions we can assume $h(Z) \ge 0$, $E_0(\Lambda) = 0$ and
    denoting $P_\Lambda$ the projector on the groundspace of $H_\Lambda$:
    \[ 0\cdot P_\Lambda + \gap P_\Lambda^\perp = \gap P_\Lambda^\perp \le H_\Lambda \]
    \pause
    Moreover, $\gap$ is the optimal constant for which the inequality hold.
      \[
        \gap = \max\{\gamma \,|\, \gamma P_\Lambda^\perp \le H_\Lambda\}
      \]
\end{frame}

\begin{frame}{Theorem}
    If the interactions are frustration free, then either
    \begin{description}[<+->]
      \item[Gapped:] $\liminf_{\Lambda} \gap  > 0$
      \item[Gapless:]
        \[ \liminf_{\Lambda} \frac{n}{\log^{2+\epsilon} n}\; \gap = 0 \quad \forall
        \epsilon >0\] where $n = \diam \Lambda$.

      This is independent on the interactions, of $D$, of the spin dimension.
    \end{description}
      \only<3>{
    \begin{block}{Equivalently}
      If we can lower bound $\gap$ (asymptotically) by a function slower than $\frac{\log(n)^{2+\epsilon}}{n}$, then the system
      is gapped!
    \end{block}
  }
\end{frame}

\section{Sketch of the proof}

\begin{frame}{Proof outline}
  \begin{enumerate}
  \item assume that $\gap \ge C \frac{\log^{2+\epsilon} n}{n}$ for some $C, \epsilon$ \\
    for every $\Lambda$ of diameter $n$
  \item split $\Lambda = A \cup B$
  \item lower bound $\gap$ as a function of  $\gap[A]$ and $\gap[B]$
  \item iterate until we obtain a bound independent of $\Lambda$
  \end{enumerate}

  We can modify proof by only assume 1) for some ``balanced'' rectangles and obtain the $\liminf$
  restricted on that class of shapes.
\end{frame}

\begin{frame}{Recursive strategy}
  \begin{center}
    \begin{tikzpicture}[scale=0.5]
      \draw[fill=red, fill opacity=0.2] (-0.5,-0.5) rectangle (6.5,4.5);
      \draw[fill=blue, fill opacity=0.2] (3.5,-0.5) rectangle (10.5,4.5);
      \draw[<->] (-0.5,5) -- (6.5,5) node[midway, above] {$A$};
      \draw[<->] (3.5,-1) -- (10.5,-1) node[midway, below] {$B$};

      \foreach \x in {0,...,10}
      \foreach \y in {0,...,4}
      {\node [spin] (\x\y) at (\x,\y) {};}

      \foreach \x [count=\xi] in {0,...,9}
      \foreach \y in {0,...,4}
      \draw (\x\y)--(\xi\y);

      \foreach \x in {0,...,10}
      \foreach \y [count=\yi] in {0,...,3}
      \draw (\x\y) -- (\x\yi);

      \node[left] at ($ (00.west) + (-1,0) $) {$\Lambda$};

      \node<3-4>[spin,fill=red] at (2,2) {};
      \node<3-4>[spin,fill=red] at (4,3) {};
      \node<3-4>[spin,fill=red] at (0,4) {};
      \draw<3-4>[entanglement, red] (2,2) -- (4,3) -- (0,4) -- (2,2);

      \node<5>[spin,fill=green] at (6,2) {};
      \node<5>[spin,fill=green] at (9,0) {};
      \node<5>[spin,fill=green] at (7,4) {};
      \draw<5>[entanglement, green] (6,2) -- (7,4) -- (9,0) -- (6,2);

      \node<6->[spin,fill=red] (a) at (2,2) {};
      \node<6->[spin,fill=green] (b) at (9,0) {};
      \draw<6->[entanglement,red] (a) -- ($(a)!0.5!(b)$);
      \draw<6->[entanglement,green] ($(a)!0.5!(b)$) -- (b);

      %\draw<3->[<->,thick, red] (3.5,2) -- (6.5,2) node[midway, above] {$d$};
    \end{tikzpicture}
  \end{center}

  \begin{overlayarea}{\textwidth}{\textheight}
    \only<1>{
      We split $\Lambda=A\cup B$. Remember $\gap P_{\Lambda}^\perp \le H_{\Lambda}$.
      We would like to relate the gap $\gap$ with the gaps $\gap[A]$ and $\gap[B]$,
      by comparing $\expval{P^\perp_\Lambda}{\psi}$ with $\expval{H_A}{\psi}$ and $\expval{H_B}{\psi}$.
  }
  \only<2-4>{
    Let us consider an excited state $\ket{\psi} = P_{\Lambda}^\perp \ket{\psi}$. \uncover<3->{If it has excitations
    localized in $A$,} \uncover<4->{ then it will have energy at least $\gap[A]$.}
    \[ \uncover<3->{\braket{\psi} = \expval{P^\perp_A}{\psi}} \uncover<4->{ \le \frac{1}{\gap[A]} \expval{H_A}{\psi}
      \le \frac{1}{\gap[A]} \expval{H_\Lambda}{\psi}} \]
    }
    \only<5>{
      The same argument works for $B$: if excitations are localized in $B$, their energy is at least
      $\gap[B]$.
      \[ \braket{\psi} \le \expval{P^\perp_B}{\psi} \le\frac{1}{\gap[B]} \expval{H_B}{\psi}
        \le \frac{1}{\gap[B]} \expval{H_\Lambda}{\psi} \]
      }
    \only<6>{
      What if a state has excitations which are \alert{not localized} (e.~g.~ a superposition between
      excitations in $A\setminus B$ and $B\setminus A$)?
      \[  \frac{1}{\min(\gap[A],\gap[B]) }\expval{H_A + H_B}{\psi} \ge \expval{P_A^\perp}{\psi} + \expval{P_B^\perp}{\psi}  \]
  }
\end{overlayarea}
\end{frame}

\begin{frame}{Groundstates overlap}
\begin{center}
    \begin{tikzpicture}[scale=0.5]
      \draw[fill=red, fill opacity=0.2] (-0.5,-0.5) rectangle (6.5,4.5);
      \draw[fill=blue, fill opacity=0.2] (3.5,-0.5) rectangle (10.5,4.5);
      \draw[<->,visible on=<1>] (-0.5,5) -- (6.5,5) node[midway, above] {$A$};
      \draw[<->,visible on=<1>] (3.5,-1) -- (10.5,-1) node[midway, below] {$B$};

      \foreach \x in {0,...,10}
      \foreach \y in {0,...,4}
      {\node [spin] (\x\y) at (\x,\y) {};}

      \foreach \x [count=\xi] in {0,...,9}
      \foreach \y in {0,...,4}
      \draw (\x\y)--(\xi\y);

      \foreach \x in {0,...,10}
      \foreach \y [count=\yi] in {0,...,3}
      \draw (\x\y) -- (\x\yi);

      \node[left] at ($ (00.west) + (-1,0) $) {$\Lambda$};

      \node[spin,fill=red] (a) at (2,2) {};
      \node[spin,fill=green] (b) at (9,0) {};
      \draw[entanglement,red] (a) -- ($(a)!0.5!(b)$);
      \draw[entanglement,green] ($(a)!0.5!(b)$) -- (b);

      \draw<2->[<->,thick] (3.5,4.75) -- (6.5,4.75) node[midway, above] {$d$};
    \end{tikzpicture}
  \end{center}

\begin{overlayarea}{\textwidth}{\textheight}
  \only<1>{
    \begin{lemma}\vspace{-1em}
    \[ \expval{P_A^\perp}{\psi} +  \expval{P_B^\perp}{\psi} \ge (1-2\delta)\expval{P_\Lambda^\perp}{\psi} \]
    where $\delta = \norm{(P_A-P_\Lambda)(P_B-P_\Lambda)} = \norm{P_AP_B - P_\Lambda}$
    \end{lemma}
  }
    \only<2>{
      \alert{Lemma:} $\delta$ is exponentially small in $d\gap$.
      \[ \expval{P_A^\perp}{\psi} +  \expval{P_B^\perp}{\psi} \ge(1- 2e^{-c d \gap}) \expval{P_\Lambda^\perp}{\psi} \]
    }
\end{overlayarea}
\end{frame}

\begin{frame}{Recursive strategy}
    In conclusion
    \[ (1-2e^{-cd\gap})\min(\gap[A],\gap[B]) \le \expval{H_A+H_B}{\psi} .\]
    \pause
    Since $H_A+H_B = H_\Lambda + H_{A\cap B} \le 2H_\Lambda$, we get the recursive relationship
    \[
      \frac{1-2e^{-c d \gap}}{\alert<3>{2}} \min(\gap[A],\gap[B]) \le \gap.
    \]
    \pause
      Because of the factor of $\frac{1}{2}$, if we iterate then the bound will vanish exponentially fast!
\end{frame}

\begin{frame}{Averaging trick}
\begin{center}
    \begin{tikzpicture}[scale=0.5]

      \foreach \d [count=\n] in {0,1,2}{
      \draw[fill=red, fill opacity=0.2, visible on=<\n>] (-0.5,-0.5) rectangle ($ (4.5,4.5) + \d*(2,0) $);
      \draw[fill=blue, fill opacity=0.2,  visible on=<\n>]  ($ (2.5,4.5) + \d*(2,0) $)  rectangle
      (10.5,-0.5);
      \draw[fill=gray, fill opacity=0.2, visible on=<4->]  ($ (2.5,-0.5) + \d*(2,0) $) rectangle ($ (4.5,4.5) + \d*(2,0) $);
      \draw[<->, visible on=<\n>] (-0.5,5) --  ($ (4.5,5) + \d*(2,0) $) node[midway, above] {$A_\n$};
      \draw[<->, visible on=<\n>] ($ (2.5,-1) + \d*(2,0) $) -- (10.5,-1) node[midway, below] {$B_\n$};
      \draw[<->, visible on=<4->] ($ (2.5,5) + \d*(2,0) $) -- ($ (4.5,5) + \d*(2,0) $) node[midway, above]
      {\tiny $A_\n\cap B_\n$};
    }

      \foreach \x in {0,...,10}
      \foreach \y in {0,...,4}
      {\node [spin] (\x\y) at (\x,\y) {};}

      \foreach \x [count=\xi] in {0,...,9}
      \foreach \y in {0,...,4}
      \draw (\x\y)--(\xi\y);

      \foreach \x in {0,...,10}
      \foreach \y [count=\yi] in {0,...,3}
      \draw (\x\y) -- (\x\yi);

    \end{tikzpicture}
  \end{center}
  \begin{overlayarea}{\textwidth}{0.3\textheight}
      \only<-4>{
    We can split $\Lambda$ in $k$ different ways (keeping $d$ constant)
    \[ A_1 \cup B_1 = A_2 \cup B_2 = \dots = A_k \cup B_k \]
    such that $A_i \cap B_i \cap A_j \cap B_j = \emptyset$ whenever $i\neq j$
    }
\only<5>{
    \[ \frac{1}{k}\sum_{i=1}^k \qty(H_{A_i} + H_{B_i}) = H_{\Lambda} + \frac{1}{k}\sum_{i=1}^k H_{A_i \cap
        B_i} \le \qty(1+\frac{1}{k}) H_\Lambda  \]
    }

\end{overlayarea}

\end{frame}

\begin{frame}{The recursion}
  By averaging we obtain
    \[
      \frac{1-2e^{-c d \gap}}{\only<1>{2} \only<2>{1+1/k}}  \uncover<2>{\min_i} \min(\gap[{A_{\uncover<2>{i}}}],\gap[{B_{\uncover<2>{i}}}]) \le \gap
    \]
    \vspace{1em}
    \uncover<2>{
    If $\gap$ is larger than $C\frac{\log(n)^{2+\epsilon}}{n}$ then iterating this gives a constant lower
    bound.
    }
\end{frame}


\begin{frame}{Conclusion}
\begin{block}{Open question: is the threshold tight?}
    For 1D and 2D models, we know the optimal threshold is smaller:
    $\order{1/n^{3/2}}$ for open b.c. and $\order{1/n^2}$ for periodic b.c.

    (Knabe 1988; Lemm, Mozgunov 2018; Gosset, Mozgunov 2015)
  \end{block}

  \begin{center}
    \textbf{Thank you for your attention!}
  \end{center}

  \begin{block}{Reference}
    M.\ J.\ Kastoryano and A.\ L.,
    \emph{Divide and conquer method for proving gaps of frustration free Hamiltonians},
    Journal of Statistical Mechanics: Theory and Experiment, March 2018,
    \texttt{arXiv:1705.09491 [math-ph]}
\end{block}

\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
